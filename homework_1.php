<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
<table style="border-collapse: collapse;border:1px solid black;">
    <tr style="border:1px solid black;background: lightgrey;text-align: center;font-size:20px;">
        <th style="border:1px solid black;width: 200px">Число</th>
        <th style="border:1px solid black;width: 200px">Квдрат числа</th>
        <th style="border:1px solid black;width: 200px">Куб числа</th>
    </tr>
<?php
$n=10;
$style='style="border:1px solid black;width: 200px;text-align:center"';
for ($i=2;$i<=$n;$i++) {
    echo '<tr '.$style.'><td '.$style.'>'.$i.'</td><td '.$style.'>'.$i*$i.'</td><td '.$style.'>'.$i*$i*$i.'</td></tr>';
}

?>
</table>
</body>
</html>

