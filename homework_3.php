<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Домашнее задание,задача 3</title>
    <style>
        .block {
            float: left;
            width: 700px;
            height: 1000px;
            text-align: center;
        }
        .table{
            margin:0 auto;
            width: 600px;
            border-collapse: collapse;
            border:1px solid black;
        }
        .rows{
            border:1px solid black;
        }
        .tableHeader{
            border:1px solid black;
            padding: 5px;
        }
        .cols{
            border:1px solid black;
            padding: 5px;
        }

    </style>
</head>
<body>

<div class="block">
    <h1>Первый вариант вклада: ежегодный процент</h1>
    <table class="table">
        <tr class="rows">
            <th class="tableHeader">Количество лет</th>
            <th class="tableHeader">Сумма вклада</th>
        </tr>


        <?php
        //сумма
        $s=1000;
        //процент в год
        $p=0.06;
        //процент в месяц
        $m=$p/12;
        //количество лет
        $n=10;


        for($i=1;$i<=$n;$i++){
            $s=round($s+($p*$s));
            echo '<tr class="rows">
                    <td class="cols">'.$i.'</td>
                    <td class="cols">'.$s.'</td>
                  </tr>';
        }





        ?>

    </table>
</div>
<div class="block">
    <h1>Второй вариант вклада: ежемесячный процент</h1>
    <table class="table">
        <tr class="rows">
            <th class="tableHeader">Количество лет</th>
            <th class="tableHeader">Сумма вклада</th>
        </tr>
        <?php
        //сумма
        $s=1000;
        //процент в год
        $p=0.06;
        //процент в месяц
        $m=$p/12;
        //количество лет
        $n=10;


        for($i=1;$i<=$n;$i++){


            for($j=1;$j<=12;$j++){
                $s=$s+($m*$s);
            }


            echo '<tr class="rows">
                    <td class="cols">'.$i.'</td>
                    <td class="cols">'.round($s).'</td>
                  </tr>';



        }





        ?>

    </table>
</div>








</body>
</html>
