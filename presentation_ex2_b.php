<?php
$n=5;
echo '<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Задания после презентации темы об операторах циклов</title>
</head>
<body>
<table style="border: 1px solid black;border-collapse: collapse;text-align: center;padding: 5px;">
    <tr style="border: 1px solid black;">
        <th style="border: 1px solid black;width: 200px;font-size: 20px;padding: 5px;">Номер п/п</th>
        <th style="border: 1px solid black;width:200px;font-size: 20px;padding: 5px;" colspan="'.$n.'">Число</th>
    </tr>';


$rows=100;
$dif=255/($rows-1);


    for ($i=1;$i<=$rows;$i++) {

        $bgColor=abs(round(255-$i*$dif));
        $textColor=abs(round(0+$i*$dif));

        $bg="background:rgb($bgColor,$bgColor,$bgColor);";
        $text="color:rgb($textColor,$textColor,$textColor);";
        echo '<tr style="border: 1px solid black;padding: 5px;'.$bg.$text.'">
                    <td style="border: 1px solid black;padding: 5px;">' . $i . '</td>';

                    for($j=1;$j<=$n;$j++) {
                        echo '<td style="width: 200px;border: 1px solid black;padding: 5px;">' . mt_rand() . '</td>';
                    }

                  echo '</tr>';

    }












echo '</table>
</body>
</html>';

