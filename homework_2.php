<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
<table style="border-collapse: collapse;border:1px solid black;">

  <?php

  $n=18;
  $k=1;
  $style='style="border:1px solid black;width: 100px;text-align:center"';
  $styleLg='style="border:1px solid black;width: 100px;text-align:center;background:lightgrey;"';

  for ($i=1;$i<=abs(round($n/5));$i++) {
      if($i%2==0){
          echo '<tr ' . $styleLg . '>';
          for ($j = 0; $j < 5; $j++) {
              echo '<td ' . $style . '>';
              if ($k < 19) {
                  echo $k++;
              }
              echo '</td>';
          }
          echo '</tr>';
      }
      else {
          echo '<tr ' . $style . '>';
          for ($j = 0; $j < 5; $j++) {
              echo '<td ' . $style . '>';
              if ($k < 19) {
                  echo $k++;
              }
              echo '</td>';
          }
          echo '</tr>';
      }
  }
  ?>



</table>
</body>
</html>

